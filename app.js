'use strict';
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const indexRouter = require('./routes/index');
const dashboardsRouter = require('./routes/dashboards').router;
const userActionLogsRouter = require('./routes/userActionLogs').router;
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
let dotenv = require('dotenv');
dotenv.config();
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'fj9832mnsaf3j9adsa',
  resave: false, saveUninitialized: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', indexRouter);
app.use('/dashboards', dashboardsRouter);
app.use('/logs', userActionLogsRouter);

const mongoHost = process.env.MONGO_HOST || 'mongodb://localhost/dashboard';
mongoose.connect(mongoHost, { useNewUrlParser: true, useFindAndModify: false });
let db = mongoose.connection;


// Check connection
db.once('open', function() {
  console.log('Connected to MongoDB');
});

// Check for DB errose
db.on('error', function(err) {
  console.log('db error');
  console.log(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', { msg: ' 500 - Internal server error:' + err.message });
});

module.exports = app;
