"use strict";
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let userActionLog = mongoose.Schema({
  actionType: {
    type: String,
    required: true
  },
  timestamp: {
    type: Date,
    required: true,
    default: Date.now
  },
  user: {
    type: String,
    required: true
  },
  tangoDB: {
    type: String,
    default: ""
  },
  device: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  value: {
    type: Schema.Types.Mixed
  },
  argin: {
    type: String
  },
  valueBefore: {
    type: Schema.Types.Mixed
  },
  valueAfter: {
    type: Schema.Types.Mixed
  }
});

module.exports = mongoose.model("UserActionLog", userActionLog);
