'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const WIDGET = {
  canvas: String,
  id: String,
  x: Number,
  y: Number,
  height: Number,
  width: Number,
  type: { type: String },
  // Necessary since `type' is a reserved word in Mongoose
  inputs: Schema.Types.Mixed,
  order: Number,
};

let dashboardSchema = mongoose.Schema({
  _id: {
    type: ObjectId,
    auto: true,
  },
  name: {
    type: String,
    required: true,
  },
  user: {
    type: String,
    required: true,
  },
  widgets: {
    type: [WIDGET],
    required: true,
  },
  insertTime: {
    type: Date,
    default: Date.now,
  },
  updateTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  group: {
    type: String,
    required: false,
    default: null,
  },
  groupWriteAccess: {
    type: Boolean,
    required: false,
    default: false,
  },
  lastUpdatedBy: {
    type: String,
    required: false,
    default: null,
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  tangoDB: {
    type: String,
    default: "",
  },
});

module.exports = mongoose.model('Dashboard', dashboardSchema);
