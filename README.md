# Description
A API for saving and loading dashboards. The dashboards are stored in the central mongodb database.

# Install
`docker-compose up` or `npm install && npm start`

# Endpoints
Note that both enpoint assumes that the request has a cookie name `webjive_token`, which can be used to authenticate the user against the webjive redis database.
### dashboards/ GET
Gets the current dashboard, if any, for the current webjive user. 
### dashboards/ POST
Updates or creates a dashboard, depending of the presence of an dashboard_id. Returns a new (on create) or existing (on update) dashboard id.