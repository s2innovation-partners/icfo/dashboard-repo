"use strict";
/**
 * Handles urls prefixed with /logs
 */
const express = require("express");
const router = express.Router();
var UserActionLog = require("../models/userActionLog");
const decodeToken = require("./dashboards").decodeToken;

/**
 * @returns latest user action log of all or one specific device
 * @param:
 * deviceName: If supplied return logs from a specific device
 *             If not suplied return logs from all devices
 * limit: If supplied return all documents upp to this number
 */
router.get("/userActionLogs", async (req, res) => {
  const tangoDB = req.query.tangoDB || "";
  const deviceName = req.query.device ? req.query.device : /./;
  const nDocuments = parseInt(req.query.limit) || 0;
  const searchByDevice = req.query.device ? true : false;

  UserActionLog.find({tangoDB: tangoDB, device: deviceName})
    .sort({ _id: -1 })
    .limit(nDocuments)
    .exec((err, result) => {
      let data;
      if (searchByDevice) {
        data = {
          device: {
            name: deviceName,
            userActions: result
          }
        };
      } else {
        data = { userActions: result };
      }
      err ? res.sendStatus(404) : res.send(data);
    });
});

/**
 * Save user action to useractionlogs collection
 */
router.post("/saveUserAction", async (req, res) => {
  var token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username } = decoded;
  const log = req.body;
  log.username = username;
  insertLog(log, res);
});

const insertLog = (log, res) => {
  var newLog = new UserActionLog();
  newLog.actionType = log.actionType;
  newLog.tangoDB = log.tangoDB;
  newLog.user = log.username;
  newLog.device = log.device;
  newLog.name = log.name;
  newLog.timestamp = log.timestamp;
  switch (log.actionType) {
    case "ExcuteCommandUserAction":
      newLog.argin = log.argin;
      break;
    case "SetAttributeValueUserAction":
      newLog.value = log.value;
      newLog.valueBefore = log.valueBefore;
      newLog.valueAfter = log.valueAfter;
      break;
    case "PutDevicePropertyUserAction":
      newLog.value = log.value;
      break;
    case "DeleteDevicePropertyUserAction":
      break;
    default:
      break;
  }
  newLog.save(function(err) {
    if (err) {
      res.sendStatus(500);
      console.log("ERROR?");
      console.dir(err);
    } else {
      res.sendStatus(200);
    }
  });
};
module.exports.router = router;
